import os
import datetime

class Session:
    def __init__(self, id):
        self.id = id
        timestamp = '{:%y%m%d%H%M%S}'.format(datetime.datetime.now())
        self.dir = '%03d_%s' % (self.id, timestamp)
        os.makedirs('data/' + self.dir)
