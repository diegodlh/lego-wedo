#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Give as argument the parent directory with the folders containing the logs
# you would like to merge.
import glob
import sys

for path in glob.glob(sys.argv[1] + '*_test_*/'):
    uid = path.split('\\')[-2]
    lognames = glob.glob(path + 'log_*.csv')
    merge = []
    for logname in lognames:
        with open(logname) as log:
            for row in log:
                merge.append(row)
    merge = set(merge)
    with open(path + uid + '_keylog.csv', 'w') as newfile:
        for row in sorted(merge, key=lambda row: float(row.split('\t')[8])):
            newfile.write(row)
