import win32gui
import subprocess
timeout = 15
bin_path = 'C:\Program Files (x86)\LEGO Software\LEGO Education WeDo Software\WeDo.exe'


def launchWeDo():
    def enumHandler(hwnd, lParam):
        if win32gui.IsWindowVisible(hwnd):
            windows.append(win32gui.GetWindowText(hwnd))
    import time
    wedo = subprocess.Popen(bin_path)
    t0 = time.time()
    while time.time() - t0 < timeout:
        windows = []
        win32gui.EnumWindows(enumHandler, None)
        if 'Weo' in windows:
            print(time.time() - t0)
            return wedo
    print('Timeout!')
