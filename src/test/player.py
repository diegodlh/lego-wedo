# http://effbot.org/tkinterbook/tkinter-hello-again.htm
from tkinter import *
import vlc
import pdb

win_width = 320
win_height = 240
filename = 'file:///C:/peques/lego-wedo/small.mp4'

class App:

    def __init__(self, master):

        self.master = master

        video_frame = Frame(master, width=win_width, height=200)
        video_frame.pack()
        self.i = vlc.Instance()
        self.p = self.i.media_player_new(filename)
        self.p.set_hwnd(video_frame.winfo_id())

        btn_frame = Frame(master)
        btn_frame.pack(side=LEFT)
        self.playpause_btn = Button(btn_frame, image=play_icon, command=self.play)
        prev_btn = Button(btn_frame, image=prev_icon).pack(side=LEFT)
        self.playpause_btn.pack(side=LEFT)
        restart_btn = Button(btn_frame, image=restart_icon, command=self.restart).pack(side=LEFT)
        next_btn = Button(btn_frame, image=next_icon, command=self.next).pack(side=LEFT)

    def play(self):
        self.p.play()
        self.playpause_btn.configure(image=pause_icon, command=self.pause)

    def pause(self):
        self.p.pause()
        self.playpause_btn.configure(image=play_icon, command=self.play)

    def restart(self):
        self.p.stop()
        self.play()

    def next(self):
        self.p.stop()
        self.master.quit()


root = Tk()
prev_icon = PhotoImage(file='prev.gif')  # parece que debo cargar la imagen en el entorno global
play_icon = PhotoImage(file='play.gif')
pause_icon = PhotoImage(file='pause.gif')
restart_icon = PhotoImage(file='restart.gif')
next_icon = PhotoImage(file='next.gif')


root.overrideredirect(1)
win_left = root.winfo_screenwidth() - win_width
root.geometry('%dx%d+%d+0' % (win_width, win_height, win_left))

app = App(root)

root.mainloop()
root.destroy() # optional; see description below

# from tkinter import *
# import vlc
#
# win_width = 640
# win_height = 480
#
# root = Tk()
# screen_width = root.winfo_screenwidth()
# screen_height = root.winfo_screenheight()
#
# root.geometry('%dx%d+%d+0' % (win_width, win_height, screen_width - win_width))
# #root.overrideredirect(1)
# root.mainloop()
# #
# # filename = 'file:///C:/peques/lego-wedo/test-mpeg.mpg'
# # i = vlc.Instance('--no-video-deco')
# # p = i.media_player_new()
# # m = i.media_new(filename)
# # p.set_media(m)
# # p.play()
