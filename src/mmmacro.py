#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess
import os
from win32com import client
from time import sleep
from datetime import datetime
from win32gui import GetWindowText, GetForegroundWindow
import config
import pdb


class MMMacro():
    def __init__(self, dir):
        self.dir = dir
        subprocess.Popen('taskkill /im minimousemacro.exe',
                         stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # si quedó una macro de alguna instancia previa mal finalizada,
        # hace una copia de seguridad y la elimina:
        if os.path.isfile(config.mmmacro_filename):
            mtime = os.path.getmtime(config.mmmacro_filename)
            mtime = '{:%y%m%d%H%M%S}'.format(datetime.fromtimestamp(mtime))
            bkpname = os.path.dirname(config.mmmacro_filename) + mtime + '.mmmacro'
            os.rename(config.mmmacro_filename, bkpname)

        self.mmmacro = subprocess.Popen(config.mmmacro_path)
        while True:
            sleep(.1)
            if GetWindowText(GetForegroundWindow()) == 'Mini Mouse Macro':
                break
        shell = client.Dispatch("WScript.Shell")
        # comment the line below to disable mouse movement capture:
        # shell.SendKeys(config.mouse_capture_hotkey)
        shell.SendKeys(config.record_macro_hotkey)

    def terminate(self):
        dst = '%s/%s.mmmacro' % (self.dir, self.dir.split('/')[-1])
        if os.path.isfile(config.mmmacro_filename):
            os.rename(config.mmmacro_filename, dst)
        self.mmmacro.kill()
