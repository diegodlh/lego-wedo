#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Changes time stamp in session log so that it doesn't show local time, but
rather corresponding video time instead.
"""
import pdb


def getRecZero(session_ts):
    import sys
    import csv
    session_list = sys.path[0] + '/../misc/sessions.csv'
    with open(session_list) as filein:
        for row in csv.DictReader(filein):
            if row['session_ts'] == session_ts:
                return float(row['record_t0'])


def fixTime(session_ts, time, record_t0):
    from time import strptime, mktime
    time = mktime(strptime(session_ts[:6] + time, '%y%m%d%H:%M:%S'))
    time = int(time - record_t0)
    time = '{:02}:{:02}:{:02}'.format(time//3600, time%3600//60, time%60)
    return time


def main(session_log):
    import os
    import csv
    print('BETA WARNING: fixed time are only approximate. Improvements '
          'pending.')
    session_ts = os.path.splitext(os.path.basename(session_log))[0].split('_')[2]
    record_t0 = getRecZero(session_ts)
    tmp_log = session_log + '~'
    with open(session_log) as filein:
        with open(tmp_log, 'w') as fileout:
            reader = csv.reader(filein, delimiter='\t')
            writer = csv.writer(fileout, delimiter='\t')
            for row in reader:
                row_out = row
                time = row[0]
                row_out[0] = fixTime(session_ts, time, record_t0)
                event = row[1]
                if '->' in event:
                    from_trial, to_trial = event.split('->')
                    from_trial = int(from_trial) - 1
                    to_trial = int(to_trial) - 1
                    row_out[1] = '{:02d}->{:02d}'.format(from_trial, to_trial)
                writer.writerow(row_out)
    os.remove(session_log)
    os.rename(tmp_log, session_log)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Session log fixer.')
    parser.add_argument('session_log', help=('session log to fix'))
    args = parser.parse_args()
    main(args.session_log)
