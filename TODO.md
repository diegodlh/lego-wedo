Notas:
* No sirve ver si la ventana ya se creó, porque parece que se crea antes de que
  empiece el progama.

Todo:
* If user chooses "so-so" or "no" in self assessment for last trial, he is still
  asked if he wants to retry or proceed. Though it may be nice it didn't ask (as
  the user may be confused when the same level is shown after choosing to
  proceed), I would lose precisely that user's choice.
* ctypes.windll.kernel32.DebugActiveProcess()
* Por qué WeDo se abre de vuelta después de cerrarse la primera vez que lo abro en el día?
* Establecer configuración alternativa de máscaras para monitor soporte (o de
  última se usará la misma resolución).
* La única forma que conseguí de asegurar que último proyecto se grabe, es
  abriendo uno vacío (placeholder) antes de cerrar (y aún así no funciona cuando
    crashea)
* Eliminar proyecto residual que crea WeDo cuando se abre
* Desactivar: ctrl-alt-supr, pwr y tapa (auto?), teclas de fc
* Secuencia del teclado que desactiva AHK.
* Mejorar propiedad played de los trials: que haya pasado tiempo mínimo, o que haya perdido foco el player
* Incorporar función showDivider a la clase Player para que el timer registre
  cuando desaparece el divider, y no antes.
* Mejorar log para que escriba csv, con encabezado.
* Escribir log final: tiempo hasta siguiente, tiempo total jugado, autoreporte,
  reintenta/continua, reintentos (llega y ventana pierde foco)
* Pedir que reporte si pudo copiar el video del último trial abierto cuando se
  acaba el tiempo.
* Cuando el juego se pone en pausa, no se detiene el timer del trial!
* La salida a la terminal (al menos los mensajes de error, advertencias, etc) debería
  guardarse TAMBIÉN en un archivo para su consulta en caso de error. Esto podría
  implementarse con librería logging (debug). Sería útil guardar el valor de ciertas
  variables en caso de salida inesperada también.
* La pausa del timeup podría ser más evidente para el experimentador que observa desde
  afuera; por ejemplo, podría aparecer un ícono de pausa en algún sector de la pantalla
  (pero sería muy obvio para los chicos que podrían cambiar su comportamiento en sesiones
  posteriores sabiendo que el tiempo de juego es limitado), o aumentar la opacidad de
  la ventana superpuesta.
