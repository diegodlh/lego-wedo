#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import time, sleep
from datetime import datetime
import config
import subprocess
import tkinter as tk
import vlc
import win32api
import win32con
import recorder2
from win32com import client
from keyLogger import KeyLogger
from threading import Thread
import pdb
# Estas dos variables globales se declaran como workaround para correr código
# obligatorio en caso de error inesperado:
session = None
wedoprc = None
# recording flag to cleanup recorder only if recorder was launched:
recording = False


class Session:
    def __init__(self, mode, id, recovery_uid, archive_mode, custom_ts, save_path):
        from os import mkdir
        from os.path import exists
        from os.path import abspath
        from os import rename
        # from mmmacro import MMMacro
        self.id = id
        self.archive_mode = archive_mode
        if mode == 1:
            self.mode = 'teach'
            length = 1
        elif mode == 2:
            self.mode = 'train'
            length = config.train_length
        elif mode > 2:
            if mode == 3:
                self.mode = 'test'
            elif mode == 4:
                self.mode = 'retest'
            length = config.test_length
        if custom_ts:
            timestamp = custom_ts
        else:
            timestamp = '{:%y%m%d%H%M%S}'.format(datetime.now())
        self.uid = '%03d_%s_%s' % (self.id, self.mode, timestamp)
        self.dir = save_path + '/' + self.uid
        if exists(self.dir):
            bkp_path = self.dir
            bkp_index = 0
            while exists(bkp_path):
                bkp_index += 1
                bkp_path = '{}_bkp{:03d}'.format(self.dir, bkp_index)
            rename(self.dir, bkp_path)
        mkdir(self.dir)
        self.log = '%s/%s.log' % (self.dir, self.uid)
        f = open(self.log, 'a')
        f.close
        self.trials = []
        for trial in range(length):
            self.trials.append(Trial(abspath(self.dir), self.uid, self.mode,
                                     trial+1, recovery_uid, save_path))
        # self.mmmacro = MMMacro(self.dir)

    def archive(self):
        from os import rename
        from shutil import copyfile
        # self.mmmacro.terminate()
        wedo = subprocess.Popen([config.bin_path, '../empty_project.WeDo'])
        # espera a que se haya cargado anes de seguir
        try:
            wedo.wait(timeout=1)
        except subprocess.TimeoutExpired:
            pass
        for trial in self.trials:
            filename = trial.project.split('/')[-1]
            # WORKAROUND!: Agrega _archive al nombre del proyecto archivado
            # para proteger al disponible en la carpeta de WeDo en caso de
            # cierre inesperado:
            filename = '_archive.'.join(filename.split('.'))
            destination = '%s/%s' % (self.dir, filename)
            if self.archive_mode == 'move':
                try:
                    rename(trial.project,  destination)
                except:
                    copyfile(trial.project, destination)
                    print('Failed to move .WeDo file, copying instead')
            elif self.archive_mode == 'copy':
                copyfile(trial.project, destination)


class Trial:
    def __init__(self, path, uid, mode, trial_num, recovery_uid, save_path):
        from shutil import copyfile
        self.type = mode
        self.num = trial_num
        # en modo test, el primer trial es el trial 0 de ejemplo
        if self.type == 'test' or self.type == 'retest':
            self.num = trial_num - 1
        self.project = '%s/%s_trial%03d.WeDo' % (config.data_path,
                                                      uid,
                                                      self.num)
        template = '../empty_project.WeDo'
        if recovery_uid is not None:
            template = save_path + '/%s/%s_trial%03d_archive.WeDo' % (recovery_uid,
                                                                      recovery_uid,
                                                                      self.num)
        copyfile(template, self.project)
        self.video = '../videos/%s%03d.%s' % (mode, self.num,
                                              config.vidext)
        self.assessed = 0
        # elimina el assessment en los ejemplos:
        if self.type == 'train':
            self.assessed = 1
        self.played = 0
        self.self_report = None
        self.proceed = None
        self.t1 = 0
        self.ttotal = 0
        # self.keylogger = KeyLogger(path, '%s_%03d' % (uid, self.num))


class Divider(tk.Toplevel):
    def __init__(self, parent, message, called_prc, min_delay):
        tk.Toplevel.__init__(self, parent, bg='black')
        self.t0 = time()
        self.min_delay = min_delay
        self.prc = called_prc
        self.parent = parent
        self.attributes("-topmost", True)
        self.overrideredirect(1)
        # self.after(int(delay * 1000), self.destroy)
        width = win32api.GetSystemMetrics(0)
        height = win32api.GetSystemMetrics(1)
        self.geometry('%dx%d' % (width, height))
        tk.Label(self, text=message, fg='yellow', bg='black',
                 font=('Helvetica', config.divider_font_size)
                 ).pack(fill=tk.BOTH, expand=1)
        Thread(target=self.waitAndClose).start()
        self.wait_window(self)

    def waitAndClose(self):
        self.prc.wait()
        while time() - self.t0 < self.min_delay:
            pass
        self.destroy()


class Timer():
    def __init__(self, timeout, player):
        self.timeout = timeout
        self.player = player
        self.start_time = 0
        self.timer = None
        self.status = 0
        self.pop_up = None

    def start(self):
        from threading import Timer as threadTimer
        if self.timeout >= 0:
            self.timer = threadTimer(self.timeout, self.timeUp)
            self.start_time = time()
            self.timer.start()
            self.player.writeShort('timer_started')
            print('Timer started: %d seconds left' % self.timeout)
        if self.pop_up is not None:
            self.pop_up.destroy()

    def pause(self):
        if self.timer is not None:
            self.timer.cancel()
            self.player.writeShort('timer_paused')
            self.timer = None
            self.timeout -= time() - self.start_time
            print('Timer paused: %d seconds remain' % self.timeout)
            self.popUp()

    def stop(self):
        if self.timer is not None:
            self.timer.cancel()
            self.timer = None
            self.timeout -= time() - self.start_time
            print('Timer stopped: %d seconds remained' % self.timeout)

    def timeUp(self):
        self.player.writeShort('time_up')
        self.timer = None
        self.timeout -= time() - self.start_time
        print('Time is up')
        self.popUp()

    def popUp(self):
        self.pop_up = tk.Toplevel(self.player.master)
        self.pop_up.attributes("-topmost", True, '-alpha', .75)
        self.pop_up.overrideredirect(1)
        width = win32api.GetSystemMetrics(0)
        height = win32api.GetSystemMetrics(1)
        self.pop_up.geometry('%dx%d+0+0' % (width, height))
        self.pop_up.bind('<Control-Alt-r>', lambda event: self.start())
        self.pop_up.bind('<Control-Alt-R>', lambda event: self.start())
        self.pop_up.bind('<Control-Alt-e>', lambda event: self.player.quit())
        self.pop_up.bind('<Control-Alt-E>', lambda event: self.player.quit())


class Reloader():
    def __init__(self):
        import config
        self.timer = None
        self.interval = config.project_reload_interval
        self.current_project = None
        self.wedo_bin_path = config.bin_path
        self.call = None
        self.running = False

    def load(self, project):
        import glob
        self.running = True
        for f in glob.glob(os.path.dirname(project) + '/*.current'):
            os.remove(f)
        lock = os.path.splitext(project)[0] + '.current'
        open(lock, 'w').close()
        self.current_project = project
        self.reload()
        return self.call

    def reload(self):
        from threading import Timer as threadTimer
        self.timer = threadTimer(self.interval, self.reload)
        if self.running:
            if self.interval:
                self.timer.start()
            # print('reloading')
            self.call = subprocess.Popen([self.wedo_bin_path, self.current_project])

    def stop(self):
        self.running = False


class Assessment(tk.Toplevel):
    def __init__(self, parent, title=None):
        tk.Toplevel.__init__(self, parent)
        self.parent = parent
        self.attributes("-topmost", True)
        self.overrideredirect(1)
        width = win32api.GetSystemMetrics(0)
        height = win32api.GetSystemMetrics(1)
        self.geometry('%dx%d' % (width, height))
        self.box1 = tk.Frame(self)
        yes_icon = tk.PhotoImage(file='../icons/si.gif')
        neutral_icon = tk.PhotoImage(file='../icons/masomenos.gif')
        no_icon = tk.PhotoImage(file='../icons/no.gif')
        retry_icon = tk.PhotoImage(file='../icons/volver.gif')
        proceed_icon = tk.PhotoImage(file='../icons/seguir.gif')
        tk.Label(self.box1, text='¿PUDISTE COPIAR EL VIDEO?\n',
                 font=('Verdana', 40, 'bold')).pack()
        btnbox1 = tk.Frame(self.box1)
        tk.Button(btnbox1, image=yes_icon, command=self.yes
                  ).pack(side=tk.LEFT)
        tk.Button(btnbox1, image=neutral_icon, command=self.neutral
                  ).pack(side=tk.LEFT)
        tk.Button(btnbox1, image=no_icon, command=self.no
                  ).pack(side=tk.LEFT)
        btnbox1.pack()
        self.box2 = tk.Frame(self)
        tk.Button(self.box2, image=retry_icon, command=self.retry
                  ).pack(side=tk.LEFT)
        tk.Button(self.box2, image=proceed_icon, command=self.proceed
                  ).pack(side=tk.LEFT)
        self.box1.pack(anchor=tk.CENTER, expand=1)
        self.response = [None, None]
        self.wait_window(self)

    def yes(self):
        self.response[0] = 1
        self.response[1] = 1
        self.destroy()

    def neutral(self):
        self.response[0] = 0
        self.box1.destroy()
        self.box2.pack(fill=tk.Y, expand=1)

    def no(self):
        self.response[0] = -1
        self.box1.destroy()
        self.box2.pack(fill=tk.Y, expand=1)

    def proceed(self):
        self.response[1] = 1
        self.destroy()

    def retry(self):
        self.response[1] = 0
        self.destroy()


class Player:
    def __init__(self, master, trials, log, start_with, timeout):
        self.master = master
        self.master.bind('<Control-Alt-e>', lambda event: self.quit())
        self.master.bind('<Control-Alt-E>', lambda event: self.quit())
        # when firstTrial button is disabled, this provides a shortcut:
        self.master.bind('<Control-Alt-f>', lambda event: self.firstTrial())
        self.master.bind('<Control-Alt-F>', lambda event: self.firstTrial())
        self.master.bind('<Control-Alt-p>', lambda event: self.timer.pause())
        self.master.bind('<Control-Alt-P>', lambda event: self.timer.pause())
        self.timer = Timer(timeout * 60, self)
        self.reloader = Reloader()
        self.trials = trials
        self.log = log
        self.trial_timer = 0
        # establece las dimensiones del reproductor
        self.screen_width = master.winfo_screenwidth()
        self.screen_height = master.winfo_screenheight()
        self.width = int(self.screen_width * 1/3)
        self.height = int(self.screen_height * 1/2)
        self.master.geometry('%dx%d+%d+%d' % (self.width, self.height,
                                              self.screen_width - self.width,
                                              0))
        # área de título del trial
        self.title = tk.Label(master, text='NO TITLE', font=('Helvetica', 12))
        self.title.pack()
        # carga los íconos de los botones
        # self.first_icon = tk.PhotoImage(file='../icons/first.gif')
        self.prev_icon = tk.PhotoImage(file='../icons/prev.gif')
        self.play_icon = tk.PhotoImage(file='../icons/play.gif')
        self.pause_icon = tk.PhotoImage(file='../icons/pause.gif')
        self.restart_icon = tk.PhotoImage(file='../icons/restart.gif')
        self.next_icon = tk.PhotoImage(file='../icons/next.gif')
        # obtiene la altura maxima de los iconos y la usa para definir el area
        # de los botones
        btn_height = max(  # self.first_icon.height(),
                         self.prev_icon.height(),
                         self.play_icon.height(),
                         self.pause_icon.height(),
                         self.restart_icon.height(),
                         self.next_icon.height())
        btn_frame = tk.Frame(master, height=btn_height)
        btn_frame.pack(side=tk.BOTTOM)
        # configura los botones en el área correspondiente
        # self.first_btn = tk.Button(btn_frame, image=self.first_icon,
        #                            command=self.firstTrial)
        self.prev_btn = tk.Button(btn_frame, image=self.prev_icon,
                                  command=self.prevTrial)
        self.playpause_btn = tk.Button(btn_frame, image=self.play_icon,
                                       command=self.play)
        self.restart_btn = tk.Button(btn_frame, image=self.restart_icon,
                                     command=self.restart)
        self.next_btn = tk.Button(btn_frame, image=self.next_icon,
                                  command=self.nextTrial)
        # self.first_btn.pack(side=tk.LEFT)
        self.prev_btn.pack(side=tk.LEFT)
        # quick solution to add space after prev button:
        tk.Frame(btn_frame, width=btn_height).pack(side=tk.LEFT)
        self.playpause_btn.pack(side=tk.LEFT)
        self.restart_btn.pack(side=tk.LEFT)
        # quick solution to add space before next button:
        tk.Frame(btn_frame, width=btn_height).pack(side=tk.LEFT)
        self.next_btn.pack(side=tk.LEFT)
        # configura la pantalla del reproductor
        video_frame = tk.Frame(master, background='black')
        video_frame.pack(fill=tk.BOTH, expand=1)
        self.player = vlc.Instance().media_player_new()
        self.player.set_hwnd(video_frame.winfo_id())
        player_events = self.player.event_manager()
        player_events.event_attach(vlc.EventType.MediaPlayerEndReached,
                                   self.endReached)
        # inicializa el índice de trial vigente
        self.current_trial = start_with - 1

    def loadTrial(self, trial_index):
        # detiene el keylogger del trial anterior, si existe
        if self.current_trial > -1:
            # self.trials[self.current_trial].keylogger.stop()
            KeyLogger.stopTrial(KeyLogger)
        # obtiene el trial que se va a cargar
        self.current_trial = trial_index
        trial = self.trials[self.current_trial]
        # carga el video correspondiente
        self.player.set_mrl(trial.video)
        # establece el título correspondiente
        if trial.type == 'train':
            trial_type = 'EJEMPLO'
        elif trial.type == 'test' or trial.type == 'retest':
            trial_type = 'JUEGO'
            # en el modo test, el trial índice 0 es un trial de ejemplo
        title = '%s %d' % (trial_type, trial.num)
        self.title.configure(text=title)
        # abre el proyecto correspondiente
        project_call = self.reloader.load(trial.project)
        # project_call = subprocess.Popen([config.bin_path, trial.project])
        # muestra la pantalla divisora
        Divider(self.master, title, project_call, 1)
        # reinicia el botón play
        self.playpause_btn['state'] = tk.ACTIVE
        self.playpause_btn.configure(image=self.play_icon, command=self.play)
        # habilita y deshabilita botones del reproductor según corresponda
        if trial.num <= 1:
            # self.first_btn['state'] = tk.DISABLED
            self.prev_btn['state'] = tk.DISABLED
        else:
            # self.first_btn['state'] = tk.ACTIVE
            self.prev_btn['state'] = tk.ACTIVE
        if self.current_trial + 1 == len(self.trials) and trial.assessed:
            # desactiva el boton 'siguiente' si es último trial y fue evaluado
            self.next_btn['state'] = tk.DISABLED
        else:
            self.next_btn['state'] = tk.ACTIVE
        self.master.update()
        # ProjectInitializer(client.Dispatch("WScript.Shell").SendKeys)
        # primer click suelta cualquier bloque que pudiera ser arrastrado de
        # trial anterior; segundo click despliega pantalla:
        click(config.screen_offset)
        click(config.screen_offset)
        resetMotor(wedoprc.pid, .5)
        if (self.trials[self.current_trial].type in ['test', 'retest'] and
                self.trials[self.current_trial].num == 1 and
                not self.trials[self.current_trial].played):
                    self.timer.start()
        # incrementa el contador de las veces en que el trial fue abierto
        self.trials[self.current_trial].played += 1
        # self.trials[self.current_trial].keylogger.start()
        KeyLogger.startTrial(KeyLogger)
        self.trial_timer = time()

    def assessTrial(self, upcoming_trial):
        self.trial_timer = time()
        self.trials[self.current_trial].assessed = 1
        assessment = Assessment(self.master)
        self.master.update()
        self.trials[self.current_trial].self_report = assessment.response[0]
        self.trials[self.current_trial].proceed = assessment.response[1]
        with open(self.log, 'a') as f:
            timestamp = '{:%H:%M:%S}'.format(datetime.now())
            trial_num = self.current_trial + 1
            elapsed_time = time() - self.trial_timer
            line = '%s\t%02d->%02d\t%7.2f\t%d\t%d\n' % (timestamp, trial_num,
                                                        upcoming_trial+1,
                                                        elapsed_time,
                                                        assessment.response[0],
                                                        assessment.response[1])
            f.write(line)
        if self.current_trial + 1 == len(self.trials):
            self.next_btn['state'] = tk.DISABLED
        if assessment.response[1] and upcoming_trial + 1 <= len(self.trials):
            self.loadTrial(upcoming_trial)
        else:
            self.trial_timer = time()  # restablece contador si sujeto reintenta

    def writeLog(self, upcoming_trial):
        timestamp = '{:%H:%M:%S}'.format(datetime.now())
        elapsed_time = time() - self.trial_timer
        if not self.trials[self.current_trial].t1:
            self.trials[self.current_trial].t1 = elapsed_time
        self.trials[self.current_trial].ttotal += elapsed_time
        trial_num = self.current_trial + 1
        trial = self.trials[self.current_trial]
        log = open(self.log, 'a')
        log.write('%s\t%02d->%02d\t%7.2f\t%7.2f\t%03d\n' % (timestamp,
                                                            trial_num,
                                                            upcoming_trial+1,
                                                            elapsed_time,
                                                            trial.ttotal,
                                                            trial.played))
        log.close()

    def writeShort(self, flag):
        timestamp = '{:%H:%M:%S}'.format(datetime.now())
        log = open(self.log, 'a')
        log.write(timestamp + '\t' + flag + '\n')
        log.close()

    def play(self):
        self.writeShort('play')
        self.restart_btn['state'] = tk.ACTIVE
        self.player.play()
        self.playpause_btn.configure(image=self.pause_icon, command=self.pause)

    def pause(self):
        self.writeShort('pause')
        self.player.pause()
        self.playpause_btn.configure(image=self.play_icon, command=self.play)

    def endReached(self, event):
        # self.player.stop()  # won't work: https://forum.videolan.org/viewtopic.php?t=130714
        self.playpause_btn.configure(image=self.play_icon, command=self.play)
        self.playpause_btn['state'] = tk.DISABLED

    def restart(self):
        # desactiva el botón para evitar pulsaciones repetidas
        self.writeShort('replay')
        self.restart_btn['state'] = tk.DISABLED
        self.player.stop()
        self.playpause_btn['state'] = tk.ACTIVE
        self.play()

    def firstTrial(self):
        self.writeShort('revision')
        self.player.stop()
        self.timer.stop()
        # suponiendo que firstTrial se usa sólo en modo test, regresa al trial
        # 1 en vez de al 0, porque es el primer trial test no ejemplo:
        upcoming_trial = 1
        # upcoming_trial = 0
        # conserva en la lista de trials sólo los jugados:
        played_trials = [trial for trial in self.trials if trial.played]
        self.trials = played_trials
        # marca como evaluado el trial vigente:
        for t, trial in enumerate(self.trials):
            self.trials[t].assessed = 1
        self.writeLog(upcoming_trial)
        # if self.trials[self.current_trial].assessed == 0:  # enabled
        if self.trials[self.current_trial].assessed == -1:  # disabled
            self.assessTrial(upcoming_trial)
        else:
            self.loadTrial(upcoming_trial)

    def prevTrial(self):
        # desactiva el botón para evitar pulsaciones repetidas
        # self.prev_btn['state'] = tk.DISABLED
        self.player.stop()
        upcoming_trial = self.current_trial - 1
        self.writeLog(upcoming_trial)
        # if self.trials[self.current_trial].assessed == 0:  # enabled
        if self.trials[self.current_trial].assessed == -1:  # disabled
            self.assessTrial(upcoming_trial)
        else:
            self.loadTrial(upcoming_trial)

    def nextTrial(self):
        # desactiva el botón para evitar pulsaciones repetidas
        # self.next_btn['state'] = tk.DISABLED
        self.player.stop()
        upcoming_trial = self.current_trial + 1
        self.writeLog(upcoming_trial)
        if self.trials[self.current_trial].assessed == 0:
            self.assessTrial(upcoming_trial)
        else:
            self.loadTrial(upcoming_trial)

    def quit(self):
        self.player.stop()
        self.timer.stop()
        self.reloader.stop()
        self.writeLog(-1)
        self.master.destroy()
        # finaliza el keylogger del trial vigente
        # self.trials[self.current_trial].keylogger.stop()
        KeyLogger.stopTrial(KeyLogger)


class Masker:
    def __init__(self, master, mask_list):
        self.masks = {}
        for mask in mask_list:
            name = mask[0]
            x1 = mask[1]
            y1 = mask[2]
            x2 = mask[3]
            y2 = mask[4]
            color = mask[5]
            geometry = '%dx%d+%d+%d' % (x2-x1, y2-y1, x1, y1)
            this_mask = tk.Toplevel(master)
            if color:
                this_mask.configure(background=color)
            else:
                this_mask.attributes('-alpha', .01)
            this_mask.geometry(geometry)
            this_mask.attributes("-topmost", True)
            this_mask.overrideredirect(1)
            self.masks[name] = this_mask


class Resetter:
    def __init__(self, master, wedo_pid):
        self.wedo_pid = wedo_pid
        x1 = config.stop_button[0]
        y1 = config.stop_button[1]
        x2 = config.stop_button[2]
        y2 = config.stop_button[3]
        geometry = '%dx%d+%d+%d' % (x2-x1, y2-y1, x1, y1)
        self.mask = tk.Toplevel(master)
        self.mask.attributes('-alpha', .01, '-topmost', True)
        self.mask.geometry(geometry)
        self.mask.overrideredirect(1)
        self.mask.bind('<Button-1>', lambda event: self.click())

    def click(self):
        self.mask.withdraw()
        cursor_coords = win32api.GetCursorPos()
        # make sure mask disappeared before clicking
        sleep(config.stop_click_delay)
        # it is not waiting until mask disappears what matters only, but also
        # making sure the user has released the mouse button before it is
        # pressed and realeased automatically, i.e. before the mask is put
        # again
        click(cursor_coords)
        shell = client.Dispatch("WScript.Shell")
        # pulsa la letra k para ejecutar la rutina que borra la pantalla
        shell.SendKeys('{k}')
        # shell.SendKeys('{w}')
        self.mask.deiconify()
        resetMotor(self.wedo_pid, .5)


class Escaper():
    """Proporciona una vía de escape en el modo teaching"""
    def __init__(self, master):
        self.master = master
        self.master.bind('<Control-Alt-e>', lambda event: self.quit())
        self.master.bind('<Control-Alt-E>', lambda event: self.quit())
        width = win32api.GetSystemMetrics(0)
        self.master.geometry('%dx%d+%d+%d' % (config.escaper_width,
                                              config.escaper_height,
                                              width - config.escaper_width,
                                              0))
        self.master.config(bg=config.escaper_color)

    def quit(self):
        self.master.destroy()


class ProjectInitializer(Thread):
    """Este Thread despliega la pantalla, configura la velocidad del motor y
    lo reinicia cuando encuentra el pixel que le indica que se cargó el
    proyecto."""
    def __init__(self, sendkeys):
        Thread.__init__(self)
        self.daemon = True
        self.start()
        self.SendKeys = sendkeys

    def run(self):
        global wedoprc
        while True:
            pixel_color = getPixelColor(config.hint_pixel_coords)
            if pixel_color == config.hint_pixel_color:
                break
            sleep(config.hint_pixel_refresh)
        click(config.screen_offset)
        # pulsa la letra w para ejecutar la rutina que configura veloc. motor
        # self.SendKeys('{w}')
        resetMotor(wedoprc.pid, .5)


def getPixelColor(coords):
    """Based on http://rosettacode.org/wiki/Color_of_a_screen_pixel#Python"""
    import win32gui
    i_x = coords[0]
    i_y = coords[1]
    i_desktop_window_id = win32gui.GetDesktopWindow()
    i_desktop_window_dc = win32gui.GetWindowDC(i_desktop_window_id)
    long_color = win32gui.GetPixel(i_desktop_window_dc, i_x, i_y)
    i_color = int(long_color)
    rgb_color = ((i_color & 0xff),
                 ((i_color >> 8) & 0xff),
                 ((i_color >> 16) & 0xff))
    return '#%02x%02x%02x' % rgb_color


def launchWeDo(project=''):
    global wedoprc
    width = win32api.GetSystemMetrics(0)
    height = win32api.GetSystemMetrics(1)
    loader = tk.Tk()
    loader.attributes("-topmost", True)
    loader.overrideredirect(1)
    loader.geometry('%dx%d' % (width, height))
    loader.configure(bg='black')
    tk.Label(loader, text='CARGANDO...', fg='yellow', bg='black',
             font=('Helvetica', config.divider_font_size)).pack(fill=tk.BOTH,
                                                                expand=1)
    loader.after(config.launch_wait * 1000, loader.destroy)
    # si no abro el programa primero, no abre el archivo que le paso como arg
    wedoprc = subprocess.Popen(config.bin_path)
    taskbarClose()
    loader.mainloop()
    # click(config.arrow_offset)
    try:
        subprocess.Popen([config.bin_path, project]
                         ).wait(config.loading_project_delay)
    except subprocess.TimeoutExpired:
        pass
    click(config.screen_offset)
    centerCursor()


def focusLost(event):
    """Ubica arriba de todo el widget Tk que generó el argumento evento."""
    print('Foco perdido')  # debugging
    # ambas opciones son válidas:
    # event.widget.attributes("-topmost", True)
    event.widget.lift()


def taskbarClose():
    import os
    os.system('taskkill /f /im explorer.exe')


def taskbarStart():
    import os
    os.system('start explorer.exe')


def click(coords, delay=0):
    from win32gui import GetCursorPos
    prev_pos = GetCursorPos()
    x = coords[0]
    y = coords[1]
    win32api.SetCursorPos(coords)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
    sleep(delay)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)
    win32api.SetCursorPos(prev_pos)


def centerCursor():
    width = win32api.GetSystemMetrics(0)
    height = win32api.GetSystemMetrics(1)
    win32api.SetCursorPos((int(width/2), int(height/2)))


def lockCaps(flag):
    if not win32api.GetKeyState(win32con.VK_CAPITAL) == flag:
        shell = client.Dispatch("WScript.Shell")
        shell.SendKeys('{CAPSLOCK}')


def lockNum(flag):
    if not win32api.GetKeyState(win32con.VK_NUMLOCK) == flag:
        shell = client.Dispatch("WScript.Shell")
        shell.SendKeys('{NUMLOCK}')


def resetMotor(wedo_pid=None, debug_delay=0):
    import wedo
    from ctypes import windll
    # exits if no wedo devices are found:
    if not wedo.scan_for_devices():
        return
    wd = wedo.WeDo()
    d = [d for d in [wd.distance for i in range(50)] if d is not None]  # sensor may read None sometimes: check more than once
    if d:
        d = d[0]
    else:
        d = 0  # distance defaults to 0 if sensor can't be read
    if d > config.sensor_threshold:
        if wedo_pid is not None:
            # waits for previous wedo routines to finish before pausing it
            sleep(debug_delay)
            windll.kernel32.DebugActiveProcess(wedo_pid)
        # one hub's slot is motor_a, the other is motor_b
        setattr(wd, config.motor, 80)
        t0 = time()
        while (time() - t0) < config.motor_reset_timeout:
            time() - t0
            d = wd.distance
            if d is not None:
                if d <= config.sensor_threshold:
                    break
        setattr(wd, config.motor, -20)
        sleep(.2)
        setattr(wd, config.motor, 0)
        if wedo_pid is not None:
            windll.kernel32.DebugActiveProcessStop(wedo_pid)


def wipeProjectFolder():
    """Wipes the WeDo project folder contents"""
    from glob import glob
    from os import remove
    project_folder = config.data_path
    for file in glob(project_folder + '\*'):
        remove(file)


def wipePresentFile():
    from os.path import exists
    from os import remove
    # removes .ini file which stores last WeDo file opened
    if exists(config.present_file):
        remove(config.present_file)

# def abrirProyecto(nombre_proyecto, latency=.5):
#     """Abre el proyecto 'nombre_proyecto'; espera el valor en segundos
#     indicadoentre pasos de la operación."""
#     click(config.menu_offset)
#     time.sleep(latency)
#     click(config.open_offset)
#     time.sleep(latency)
#     shell = win32com.client.Dispatch("WScript.Shell")
#     shell.SendKeys(nombre_proyecto)
#     shell.SendKeys('{ENTER}')
#
#
# def nuevoProyecto():
#     click(config.menu_offset)
#     time.sleep(.5)
#     click(config.new_offset)
#
#
# def salir():
#     click(config.menu_offset)
#     time.sleep(.3)
#     click(config.quit_offset)
#
#
def main(recovery_uid, id, mode, start_with, archive_mode, no_ahk, custom_ts,
         timeout, save_path):
    if not recovery_uid:
        print('Pase un UID como argumento al launcher (UID: id_modo_timestamp)\n' +
              'para usar los proyectos archivados de dicho UID, en vez del proyecto vacío.\n')
    if not mode:
        while True:
            try:
                mode = int(input('Ingrese el modo de ejecución: ' +
                                 '1. Teaching, 2. Training, 3. Test, 4. Retest\nModo: '))
                if mode in [1, 2, 3, 4]:
                    break
            except ValueError:
                pass
    if not id:
        while True:
            try:
                id = int(input('id: '))
                break
            except ValueError:
                pass
    global session
    # wipe project folder to reduce load on recorder2's file monitor:
    # wipeProjectFolder()
    # remove .ini file which stores last WeDo file opened:
    wipePresentFile()
    session = Session(mode, id, recovery_uid, archive_mode, custom_ts, save_path)
    # starts screen capturing if not in teach or train modes:
    # if session.mode not in ['teach', 'train']:
    if True:
        global recording
        recording = True
        recorder2.record(session.uid)
    lockCaps(1)
    lockNum(1)
    # ejecuta el script AutoHoyKey que desactiva teclas y combinaciones:
    if no_ahk:
        print('Running in test mode. Unrestricted user interaction')
    else:
        subprocess.Popen([config.ahk_path, config.ahk_script])
    # establece el volumen y el brillo de la pantalla predefinidos:
    subprocess.Popen(['nircmd', 'mutesysvolume', str(0)])
    subprocess.Popen(['nircmd', 'setsysvolume', str(config.volume/100*65535)])
    subprocess.Popen(['nircmd', 'setbrightness', str(config.brightness)])
    resetMotor()
    launchWeDo(session.trials[0].project)
    root = tk.Tk()
    root.attributes("-topmost", True)
    root.overrideredirect(1)
    # master.bind('<FocusOut>', focusLost)
    if session.mode == 'teach':
        Escaper(root)
        # # inicia el keylogger del trial único en modo teaching:
        # session.trials[0].keylogger.start()
    else:
        if timeout is None:
            timeout = config.session_timeout
        player = Player(root, session.trials, session.log, start_with, timeout)
        root.after(config.t1_delay, lambda: player.loadTrial(start_with))
    Masker(root, config.masks)
    Resetter(root, wedoprc.pid)
    with open(session.log, 'a') as f:
        f.write('{:%H:%M:%S}\tstart\n'.format(datetime.now()))
    root.mainloop()
    try:
        root.destroy()
    except tk.TclError:
        pass  # ataja el error si no pudo destruir la ventana, y no hace nada


if __name__ == '__main__':
    import sys
    import os
    import argparse
    parser = argparse.ArgumentParser(description='Lego WeDo launcher.')
    parser.add_argument('recovery_uid', nargs='?',
                        help=('given as id_mode_timestamp; use stored .WeDo '
                              'projects instead of default empty files'))
    parser.add_argument('--uid', type=int,
                        help=('user\'s ID'))
    parser.add_argument('--mode', type=int, choices=range(1, 5),
                        help=('1. Teaching, 2. Training, 3. Test, 4. Retest'))
    parser.add_argument('--timeout', type=float,
                        help='provide alternative session timeout in minutes')
    parser.add_argument('--start-with', type=int, choices=range(0, 11),
                        default=0, help='trial to start with')
    parser.add_argument('--archive', choices=['move', 'copy'], default='move',
                        help='move or copy project files on exit')
    parser.add_argument('--custom-ts', help='provide an alternative \
                                             custom timestamp for the session')
    parser.add_argument('--test', action='store_true',
                        help='disables interaction restrictions')
    parser.add_argument('--reset-latency', type=float,
                        help=('overrides the resetter latency defined in the '
                              'config file'))
    parser.add_argument('--save-path', help=('specifies were project files '
                                             'and session log will be saved'))
    args = parser.parse_args()
    if args.save_path:
        save_path = os.path.abspath(args.save_path)
    else:
        save_path = '../data'
    os.chdir(sys.path[0])
    if args.reset_latency:
        config.stop_click_delay = args.reset_latency
    try:
        sys.exit(main(args.recovery_uid, args.uid, args.mode, args.start_with,
                      args.archive, args.test, args.custom_ts, args.timeout, save_path))
    finally:
        if recording:
            recorder2.cleanup()
        session.archive()
        with open(session.log, 'a') as f:
            f.write('{:%H:%M:%S}\tend'.format(datetime.now()))
        # KeyLogger.stop(KeyLogger)
        if not args.test:
            client.Dispatch("WScript.Shell").SendKeys(config.ahk_quit)
        lockCaps(0)
        taskbarStart()
        wedoprc.kill()
        resetMotor()
