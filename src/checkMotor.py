#!/usr/bin/env python
# -*- coding: utf-8 -*-
import wedo
from time import sleep


def checkMotor(speed, delay):
    wd = wedo.WeDo()
    wd.motor_b = speed
    try:
        while True:
            print(wd.distance)
            sleep(delay)
    except KeyboardInterrupt:
        pass
    wd.motor_b = 0


def main():
    speed = int(sys.argv[1])
    delay = float(sys.argv[2])
    checkMotor(speed, delay)

if __name__ == '__main__':
    import sys
    sys.exit(main())
