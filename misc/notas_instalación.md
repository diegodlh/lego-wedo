* instalo paquete keyboard-layout para atom, para que en la versión 1.11.2
  funcione alt-gr.
* no funciona opción canvaslock en wedo.ini. Para que funcione, instalo una versión actualizada de WeDo que me envían
  desde el soporte de Lego en EEUU: v1.2.3
* instalo http://www.bricelam.net/ImageResizer/
* pip install https://github.com/itdaniher/WeDoMore/archive/master.zip; permite operar con dispositivos WeDo desde Python.
* https://sourceforge.net/projects/libusb/files/libusb-1.0/libusb-1.0.20/libusb-1.0.20.7z/download
  y copio libusb-1.0.dll a system32
* instalo drivers webcam c920 logitech. Uso el software Logitech Webcam Software, por un lado para configurar las cámaras
  como se indica en la sección "webcam". Por otro lado, también lo uso para verificar el correcto posicionamiento de las
  cámaras, tanto el primer día como antes de empezar con las sesiones cada día.
* ~~yawcam y jre~~
* agrego la ruta de mi software y de VLC a la variable de entorno path
* instalo ffmpeg y agrego la ruta a path
* hay mejores formas de controlar el volumen de Windows con Python a través de
  pywin32, pero eran complejas. Descargo nircmd que hace eso, entre otras cosas,
  y lo agrego al path.
* Instalo AutoHotKey y agrego la ruta a la variable de entorno del sistema PATH
* Establezco la configuración del puntero Windows a "Aero Extra Grande"
* Para evitar que el touchpad sea activado con Fn+F6, no tuve opción sino
  desinstalar los controladores. No obstante, me terminé topando con un problema que
  ahora no recuerdo exactamente y decidí reinstalarlos, inhabilitar el touchpad y
  pedir que no lo vuelva a activar después de reiniciar (es decir, que recuerde el estado anterior).
  El touchpad puede igulamente ser activado con la secuencia Fn+F6.
  Ni siquiera remapeando la tecla en el registro:
    [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Keyboard Layout]
    "Scancode Map"=hex:00,00,00,00,00,00,00,00,02,00,00,00,00,00,73,e0,00,00,00,00
    (ver SharpKeys)
  pude evitar que sea interpretada por Synaptics.

===
Acceso remoto
* Cambio la clave de registro HKLM\System\CurrentControlSet\services\SharedAccess\Parameters\StandaloneDhcpAddress
  a 192.168.10.1 para que el gateway del hotspot esté ahí: http://superuser.com/questions/564103/how-to-change-the-dhcp-range-alloted-through-ics-in-windows-7-to-192-168-1-x-ran/564111#564111
* Tuve algunos problemas y probé con software de terceros (como Virtual Router), pero descubrí que
  los problemas se debían al estándar que estaba utilizando la placa de red (ver punto siguiente). Igualmente,
  terminé usando un router como intermediario entre notebook y tablet.
* Configuración del adaptador inalámbrico para que sea compatible con dispositivos que no soportan estándar N:
    - Preferir la banda de 2.4GHz
    - Canal Ad-Hoc 802.11b/g: 11 (no sé si es necesario)
    - No compatible con canal de 40MHz (no sé si es necesario)
    - Modo G solamente
===
Screen and mouse capture
VLC: el puntero parpadea.
CamStudio: parecía bueno, pero el par de veces que lo probé no pude recuperar el archivo grabado (como que se cuelga).
OBS: me pide instalar algo de DirectX. Parece de lo mejor disponible.
https://antumdeluge.wordpress.com/2014/02/19/recording-screen-windows-desktop-using-ffmpeg/
* Para captura de pantalla termino usando screen-capture-recorder:
https://trac.ffmpeg.org/wiki/Capture/Desktop#Windows
https://github.com/rdp/screen-capture-recorder-to-video-windows-free
ffmpeg -f dshow -i video="screen-capture-recorder":audio="Microphone" -framerate 20 -vcodec libx264 -crf 0 -preset ultrafast -acodec pcm_s16le output.mkv

con gdigrab veo un flicker del mouse: https://ffmpeg.org/ffmpeg-devices.html#gdigrab
* En cuanto a la captura del mouse, después de probar varios softwares "bonitos", terminé usando
  uno que se ajustó mejor a mis necesidades (no incluía cosas como grabar macros para reprodcirlas después):
  basicKeyLogger.

===
webcam
De forma independiente para cada cámara (con el software Logitech):
Desactivo RightLight y RightSound
Desactivo enfoque automático y configuro distancia focal a infinita
Configuro al máximo el volumen del micrófono
Desactivo los leds indicadores

En la configuración Windows,
Configuro ambos micrófonos Logitech a volumen 70 (sobreescribe configuración arriba)

En la configuración Realtek,
#Configuro micrófono de la Notebook a 70 con FFP (Far Field Pickup) solamente.
Finalmente, configuro a 50 sin FFP.
ispy:
http://localhost:8080/mjpegfeed?oid=1&full
Tengo que ver cómo hago para que Logiteh no popupee cuando uso la cámara
ispy está accediendo al stream h264 de la cámara? por qué consume tanta cpu si no?

http://www.kinovea.org/en/forum/viewtopic.php?id=736&p=2  

https://ffmpeg.zeranoe.com/forum/viewtopic.php?f=17&t=2421
https://trac.ffmpeg.org/wiki/DirectShow
  ffmpeg -list_devices true -f dshow -i dummy
ffmpeg -f dshow -list_options true -i video="Logitech HD Pro Webcam C920"
recordar que el orden de las opciones IMPORTA:
ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 1920x1080 -framerate 24 -i video="Logitech HD Pro Webcam C920" -copyinkf -c copy dump.mp4
-copyinkf
If your input video does not start with a keyframe,
the option allows you not to start stream copying
with the first keyframe (this is the defaul
behaviour), but with the first frame of the
input stream (no matter if it is a keyframe or
not).

-rtbufsize
By default if it receives a video frame "too early" (while the previous frame isn't finished yet), it will discard that frame, so that it can keep up the the real time input. You can adjust this by setting the -rtbufsize parameter,

Intenté levantar dos streams al mismo tiempo de la misma cámara, y no le gustó un carajo: pantalla azul de la muerte!

Streaming:
https://trac.ffmpeg.org/wiki/StreamingGuide
No funcionó: ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 320x240 -framerate 12 -i video="Logitech HD Pro Webcam C920" -framerate 12 -vcodec libx264 -crf 0 -preset ultrafast -listen 1 -f h264 http://localhost:8080
tampoco: ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 320x240 -framerate 24 -i video="Logitech HD Pro Webcam C920" -vcodec libx264 -crf 0 -preset ultrafast -tune zerolatency -b 900k -listen 1 -f h264 http://localhost:8080
así sí, pero tengo que saber dirección del host: ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 320x240 -framerate 12 -i video="Logitech HD Pro Webcam C920" -r 30 -vcodec mpeg4 -f mpegts udp://host:1234/
esto también: ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 320x240 -i video="Logitech HD Pro Webcam C920" -vcodec libx264 -preset ultrafast -f mpegts udp://localhost:6666
Transmitiendo un archivo, funciona así: ffmpeg -i archivo.mp4 -c copy -f mpegts http://192.168.1.5:8080, pero no funciona si uso localhost (es decir, tengo que poner la dirección del equipo que emite). Además, si el receptor termina la transmisión, se cierra la emisión.

Puedo acceder a la cámara provista por Live Stream a través de Unreal Media Stramer, con VLC rtmp, pero es muy lenta.


HAY QUE VER TEMAS DE:
BUFFER
SOBREESCRIBIR PREEXISTENTE?
framerate
QUÉ ES EL VALOR "SPEED=X"
Cómo hago para rescatar un archivo corrupto? Se colgó antes de terminar de escribir.

###
Límites a la interacción del usuario
El botón de encendido y cerrar la tapa, con batería y en AC no hacen nada (configuración fija)
Creo un perfil del administrador de energía para que la pantalla no se apague
ni oscurezca, que no se suspenda la alimentación a dispositivos USB, entre otras
opciones.
