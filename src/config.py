from os.path import expanduser

bin_path = 'C:\Program Files (x86)\LEGO Software\LEGO Education WeDo Software\WeDo.exe'
present_file = expanduser('~') + '/AppData/Roaming/LEGO Company/WeDo/WeDo.ini'  # stores WeDo's PresentFile environment variable
data_path = expanduser('~') + '/Documents/LEGO Creations/WeDo/Projects'
# predefined volume and brigthness values (0-100)
volume = 50
brightness = 50
# session timeout in minutes
session_timeout = 20
# pixel offset of the arrow revealing the lower pane
arrow_offset = (20, 700)
screen_offset = (300, 0)
menu_offset = (1300, 0)
open_offset = (1300, 50)
quit_offset = (1300, 20)
new_offset = (1300, 80)
train_length = 2
test_length = 11
player_width = 320
player_height = 240
vidext = 'avi'
# masks: [('name', x1, y1, x2, y2, 'hexcolor'), ...]
masks = [('speed', 432, 655, 488, 712, '#eaebeb'),
         ('turn_for', 540, 655, 596, 712, '#eaebeb'),
         ('tilt', 594, 655, 650, 712, '#eaebeb'),
         ('distance', 648, 655, 704, 712, '#eaebeb'),
         ('random', 810, 655, 866, 712, '#eaebeb'),
         ('loop', 1026, 655, 1107, 712, '#eaebeb'),
         # ('key', 456, 711, 522, 768, '#eaebeb'),  # wedo1.1
         ('key', 405, 711, 471, 768, '#eaebeb'),  # wedo1.2
         # ('inbox', 519, 711, 585, 768, '#eaebeb'),  # wedo1.1
         ('inbox', 468, 711, 534, 768, '#eaebeb'),  # wedo1.2
         # ('message', 583, 711, 639, 768, '#eaebeb'),  # wedo1.1
         ('message', 532, 711, 588, 768, '#eaebeb'),  # wedo1.2
         # ('mic', 637, 711, 693, 768, '#eaebeb'),  # wedo1.1
         ('mic', 586, 711, 642, 768, '#eaebeb'),  # wedo1.2
         # ('input', 691, 711, 747, 768, '#eaebeb'),  # wedo1.1
         ('input', 640, 711, 696, 768, '#eaebeb'),  # wedo1.2
         # ('increment', 799, 711, 855, 768, '#eaebeb'), # wedo1.1
         ('increment', 748, 711, 804, 768, '#eaebeb'),  # wedo1.2
         ('lights_on', 802, 711, 858, 768, '#eaebeb'),  # wedo1.2
         ('lights_off', 856, 711, 912, 768, '#eaebeb'),  # wedo1.2
         # ('dialog', 853, 711, 909, 768, '#eaebeb'),  # wedo1.1
         ('dialog', 910, 711, 966, 768, '#eaebeb'),  # wedo1.2
         # ('arrow', 10, 644, 37, 655, '#ffffff'),  # wedo1.1
         ('arrow', 10, 639, 49, 655, '#ffffff'),  # wedo1.2
         ('hub', 7, 0, 115, 177, '#ffffff'),
         ('help', 118, 0, 233, 36, '#ffffff'),
         ('screen', 228, 203, 351, 253, ''),
         # screen_arrow oculta sin evitar interracion, a cargo de screen arriba
         ('screen_arrow', 317, 222, 331, 233, '#eaebeb')]
# escaper widget allows exiting from teaching mode
escaper_width = 115
# escaper_height = 32  # wedo1.1
escaper_height = 138  # wedo1.2, starts with menu visible
escaper_color = '#ffffff'
# coordinates and color of the hint pixel (to determine if wedo opened)
hint_pixel_coords = (287, 5)
hint_pixel_color = '#4fa8c2'
hint_pixel_refresh = .1
# time in seconds to wait until wedo fully loaded
launch_wait = 3
# launching project delay: delay in seconds while first project file loads:
loading_project_delay = .1
display_screen_delay = .1
# delay in microseconds between loader screen and first trial's divider
# increase this value if player and masks are on top of this divider
t1_delay = 100
# interval between project reloadings. No reloadings if set to 0
project_reload_interval = 0

divider_font_size = 50

###
# Resetter configuration
###
motor = 'motor_b'
# stop button coordinates (x1, y1, x2, y2)
stop_button = (1307, 595, 1359, 647)
sensor_threshold = 8
# timeout in seconds for motor reset
motor_reset_timeout = 2
stop_click_delay = .5

###
# MMMacro configuration
###
mmmacro_path = 'minimousemacro'
# default mmmacro filename; future versions could look it up in the registry
mmmacro_filename = 'c:/peques/lego-wedo/data/mmmacro/MMM_out.mmmacro'
mouse_capture_hotkey = '^%c'
record_macro_hotkey = '^%r'

###
# Basic Key Logger configuration
###
bkl_path = 'c:/basicKeyLogger/'
bkl_config = [
              # timestamps relative to epoch (1) or to beginning of session (0)
              'absoluteFlag=1',
              'buttonReleaseFlag=1',
              # saves log regularly, but makes computer slow
              'writePeriodS=0',
              'writeMode=append'
             ]

###
# AutoHotKey configuration
###
ahk_path = 'C:/Program Files/AutoHotkey/AutoHotKeyU64.exe'
ahk_script = './ahk.ahk'
ahk_quit = '^%+H'
