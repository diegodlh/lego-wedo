import tkinter as tk
import config
import vlc
import pdb


class Player:
    def __init__(self, master):
        from math import ceil
        self.master = master
        screen_width = master.winfo_screenwidth()
        screen_height = master.winfo_screenheight()
        self.width = int(screen_width * 1/3)
        self.height = int(screen_height * 1/2)
        self.master.geometry('%dx%d+%d+%d' % (self.width, self.height,
                                              screen_width - self.width, 0))

        self.title = tk.Label(master, text='NO TITLE', font=(12))
        self.title.pack()

        self.first_icon = tk.PhotoImage(file='../icons/first.gif')
        self.prev_icon = tk.PhotoImage(file='../icons/prev.gif')
        self.play_icon = tk.PhotoImage(file='../icons/play.gif')
        self.pause_icon = tk.PhotoImage(file='../icons/pause.gif')
        self.restart_icon = tk.PhotoImage(file='../icons/restart.gif')
        self.next_icon = tk.PhotoImage(file='../icons/next.gif')
        btn_height = max(self.first_icon.height(),
                         self.prev_icon.height(),
                         self.play_icon.height(),
                         self.pause_icon.height(),
                         self.restart_icon.height(),
                         self.next_icon.height())
        btn_frame = tk.Frame(master, height=btn_height)
        btn_frame.pack(side=tk.BOTTOM)
        self.first_btn = tk.Button(btn_frame, image=self.first_icon,
                                   command=self.firstTrial)
        self.prev_btn = tk.Button(btn_frame, image=self.prev_icon,
                                  command=self.prevTrial)
        self.playpause_btn = tk.Button(btn_frame, image=self.play_icon,
                                       command=self.play)
        self.next_btn = tk.Button(btn_frame, image=self.next_icon,
                                  command=self.nextTrial)
        self.first_btn.pack(side=tk.LEFT)
        self.prev_btn.pack(side=tk.LEFT)
        self.playpause_btn.pack(side=tk.LEFT)
        tk.Button(btn_frame, image=self.restart_icon,
                  command=self.restart).pack(side=tk.LEFT)
        self.next_btn.pack(side=tk.LEFT)

        video_frame = tk.Frame(master, background='black')
        video_frame.pack(fill=tk.BOTH, expand=1)
        self.player = vlc.Instance().media_player_new()
        self.player.set_hwnd(video_frame.winfo_id())

    def play(self):
        self.player.play()
        self.playpause_btn.configure(image=self.pause_icon, command=self.pause)

    def pause(self):
        self.player.pause()
        self.playpause_btn.configure(image=self.play_icon, command=self.play)

    def restart(self):
        self.player.stop()
        self.play()

    def firstTrial(self):
        pass

    def prevTrial(self):
        self.p.stop()
        self.session.current_trial -= 1
        self.loadTrial()

    def nextTrial(self):
        self.p.stop()
        self.session.current_trial += 1
        self.loadTrial()

    def loadTrial(self):
        """Carga el trial actual: video y proyecto."""
        mrl = '../videos/%03d.%s' % (self.session.current_trial, config.vidext)
        self.p.set_mrl(mrl)
        self.session.abrirProyecto()
        self.master.lift()


def loadIcon(image, height):
    from math import ceil
    icon = tk.PhotoImage(file=image)
    image_height = icon.height()
    if height < image_height:
        sample_ratio = ceil(image_height/height)
        icon = icon.subsample(sample_ratio)
    return icon


def focusLost(event):
    """Ubica arriba de todo el widget Tk que generó el argumento evento."""
    print('Foco perdido')  # debugging
    # ambas opciones son válidas:
    # event.widget.attributes("-topmost", True)
    event.widget.lift()


def exitKeySequence(event):
    event.widget.destroy()


root = tk.Tk()
root.attributes("-topmost", True)
root.overrideredirect(1)
root.bind('<FocusOut>', focusLost)
root.bind('<Control-Alt-e>', exitKeySequence)
player = Player(root)
root.mainloop()
