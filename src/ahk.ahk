﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


Tab:: return ; workaround que protege al player de pulsaciones repetidas con el teclado
!Tab::return
<^>!Tab::return
!Esc::return
^Esc::return
^!Esc::return
^+Esc::return
^!+Esc::return
!F4::return
^!F1::return
^!F6::return
^!F11::return
^!F12::return
Numlock::return
^Numlock::return
!Numlock::return
^!Numlock::return
<^>!Numlock::return
<#Numlock::return
CapsLock::return
^CapsLock::return
!CapsLock::return
^!CapsLock::return
<^>!CapsLock::return
<#CapsLock::return
<#P::return
<#U::return
<#L::return ; no funciona
^!Del::return ; no funciona
^!Up::return
^!Down::return
^!Left::return
^!Right::return
; combinación que finaliza el script
^!+H::ExitApp

;; falta fn pad y fn pantalla, y fn brillo
