#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Copies only chosen reproductions according to trial reproduction list"""
import csv
import os
import sys
import shutil
import pdb

target = sys.path[0] + '/../rep-data'
source = os.path.expanduser('~') + '/Droppedbox/LNI/Experimentos/Peer Tutoring/2016/Datos/reproductions'

with open(sys.path[0] + '/../misc/trial_reproduction.csv') as filein:
    for row in csv.DictReader(filein):
        session = row['session']
        trial = '{}_trial{:03d}'.format(session, int(row['trial']))
        run = row['run']
        if run == 'na':
            continue
        path = '/'.join([target, run, session])
        if not os.path.exists(path):
            os.makedirs(path)
        src = '/'.join([source, run, session, trial])
        dst = '/'.join([target, run, session, trial])
        shutil.copytree(src, dst)
        print(src + ' -> ' + dst)
