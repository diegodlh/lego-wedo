#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import shutil
import time
import datetime
import filecmp
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import pdb


class EventHandler(FileSystemEventHandler):
    def __init__(self, save_path):
        super().__init__()
        self.newest = {}
        self.current = None
        self.ts = None
        self.msg = ''
        self.save_path = save_path

    def on_created(self, event):
        if os.path.splitext(event.src_path)[1] == '.current':
            self.current = os.path.splitext(event.src_path)[0] + '.WeDo'
        else:
            self.copy(event)

    def on_modified(self, event):
        self.copy(event)

    def copy(self, event):
        self.msg += 'creation/modification noticed; '
        t = time.time()
        t = datetime.datetime.fromtimestamp(t)
        basename = os.path.basename(event.src_path)
        filename = os.path.splitext(basename)[0]
        uid = '_'.join(filename.split('_')[:-1])
        path = '/'.join([self.save_path, uid, filename])
        # path = '/'.join(['\\\\VBOXSVR\\Público', 'data', uid, filename])
        try:
            os.stat(path)
        except:
            os.makedirs(path)
        if self.ts is not None:
            dst_path = '{}/{}.WeDo'.format(path, '{:.3f}'.format(self.ts).replace('.', '_'))
        else:
            dst_path = '{}/{:%Y-%m-%d-%H-%M-%S-%f}.WeDo'.format(path, t)
        if filename in self.newest:
            self.msg += 'already tracked; '
            if filecmp.cmp(event.src_path, self.newest[filename],
                           shallow=False):
                self.msg += 'no changes; '
                return
        self.msg += 'copying; '
        try:
            shutil.copyfile(event.src_path, dst_path)
        except:
            print('Could not copy ' + event.src_path)
        self.newest[filename] = dst_path


if __name__ == "__main__":
    save_path = os.getcwd()
    default_path = os.path.expanduser('~')
    default_path += '\Documents\LEGO Creations\WeDo\Projects'
    path = sys.argv[1] if len(sys.argv) > 1 else default_path
    os.chdir(sys.path[0])
    event_handler = EventHandler(save_path)
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    pdb.set_trace()
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
