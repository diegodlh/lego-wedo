# Trial videos
This folder contains the videos presented to the study participants in each trial, that they had to reproduce by building block sequences or programs, as described in the *Trial videos* subsection of our work.[^1]

10 videos were presented in the *test* stage (`test001-010.avi`), and 10 in the *retest* stage (`retest001-010.avi`).

Videos `test000.avi` and `retest000.avi` were used by the researcher to explain the video player, trial navigator and self assessment buttons to the participant in the *Test stage*, as described in the *Procedures* subsection of our work.

Videos `train001.avi` and `train002.avi` correspond to the two example trials shown in the second part of the *Instruction stage*, as described in the *Procedures* subsection of our work.

Videos can be downloaded and reproduced using any standard video player, such as [VLC](https://www.videolan.org/vlc/).

[^1]: https://doi.org/10.31234/osf.io/46tx5