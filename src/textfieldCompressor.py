#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Compresses the textfield contents into one ASCII character.
"""
import string
from wedoReader import abbreviations

textcodes = {}
for i in range(9-0+1):
    # assigns codes 0-9 to strings 0-9, respectively:
    textcodes[str(i)] = str(i)
for i in range(20-10+1):
    # assigns codes a-k to strings 10-20, respectively:
    textcodes[str(10+i)] = chr(ord('a')+i)

textcodes[''] = '_'
textcodes['abc'] = '@'
textcodes['123'] = '#'

# common and possibly common target word mispellings
mispellings = {
    'HOLA': ['OLA', 'H0LA', '0LA'],
    'PATO': ['PAT0'],
    'RANA': [],
    'HEY': ['HEI', 'EY', 'EI'],
    'LEÓN': ['LEON', 'LE0N'],
    '321': [],
    'YA': [],
    'EL': [],
    'LICEO': ['LISEO', 'LICE0', 'LISE0'],
    'ACHÚ': ['ACHU'],
    'SALUT': ['SALU'],
    'RING': ['RIN', 'RIMG', 'RIM'],
    'AUTO': ['AUT0'],
    'UY': ['UI', 'HUY', 'HUI'],
    'AVES': ['ABES'],
    'LA': [],
    'CLASE': [],
    'UN': [],
    'OSO': ['OS0', '0SO', '0S0'],
    'BRUUM': ['BRUM', 'BRUUN', 'BRUN']
    }
mispell_inv = {typo: word
               for word, typos in mispellings.items() for typo in typos}

# create list of available ascii characters
remaining_characters = (
    string.ascii_letters +
    string.digits +
    string.punctuation
    )
remaining_characters = [char for char in remaining_characters
                        if char == 'F' or  # 'Text Plug' abbreviation 'F' not used in condensed mode
                        char not in abbreviations.values() and
                        char not in textcodes.values()]
remaining_characters.remove('w')  # reserves 'w' character for [n>0] in W[n>0]
remaining_characters.remove('*')  # reserves wildcard '*' characters
remaining_characters.remove('-')  # reserves '-' for absent textfield blocks

for word in sorted(list(mispellings.keys())):
    textcodes[word] = remaining_characters.pop(0)


def make_xlat(dictionary, brackets=False):
    """
    Given a dictionary with string -> 1-char code mapping, it returns an xlat
    closure which takes a sequence to be substituted, and returns the
    substituted string, according to the dictionary mapping.
    """
    import re
    rx = re.compile(r'\[(%s)\]' % '|'.join(map(re.escape, dictionary)))

    def one_xlat(match):
        repl = dictionary[match.group(1)]
        if brackets:
            repl = '[%s]' % repl
        return repl

    def xlat(text):
        return rx.sub(one_xlat, text)
    return xlat

# replaces strings between brackets into their corresponding codes:
string2code = make_xlat(textcodes)
# replaces typos between brackets into their corresponding correct words:
mispell2word = make_xlat(mispell_inv, brackets=True)


def compress(sequence, typos=True, transformations=True):
    import re
    if typos:
        sequence = mispell2word(sequence)

    # def subfun(match):
    #     if re.match(r'^W\[[1-9]\d*\]$', match.group(0)):
    #         # code W[n] with n>0 as 'W'
    #         return 'W'
    #     elif re.match(r'^W(\[0?\])?$', match.group(0)):
    #         # code W[0], W[] and W as 'w'
    #         return 'w'
    # sequence = re.sub(r'W(\[\d*\])?', subfun, sequence)
    sequence = re.sub(r'W\[[1-9]\d*\]', 'Ww', sequence)
    sequence = string2code(sequence)
    sequence = re.sub(r'\[.*?\]', '*', sequence)
    if transformations:
        sequence = re.sub(r'B0', 'B_', sequence)
        sequence = re.sub(r'X', '', sequence)  # when loose textfield is used, distance to target should not involve removal of plug holder
        # sequence = re.sub(r'W0', 'W_', sequence)  # W0 and W_ are not expected in correct responses
        # sequence = re.sub(r'W[1-6]', 'W4', sequence)
        # sequence = re.sub(r'W[7-9|10|11]', 'W9', sequence)
    return sequence
