import config
from win32api import keybd_event


class KeyLogger():
    def __init__(self, path, trial):
        kpc_path = '%s/%s_kpclog.csv' % (path, trial)
        log_path = '%s/%s_keylog.csv' % (path, trial)
        bkl_cmd = [config.bkl_path + 'startHidden',
                   ' '.join(config.bkl_config),
                   'kpcPath=%s' % kpc_path,
                   'logPath=%s' % log_path]
        self.bkl_cmd = ' '.join(bkl_cmd)

    def start(self):
        from subprocess import Popen
        Popen(self.bkl_cmd, cwd=config.bkl_path)

    def stop(self):
        keybd_event(240, 0)

    def startTrial(self):
        keybd_event(236, 0)

    def stopTrial(self):
        keybd_event(237, 0)
