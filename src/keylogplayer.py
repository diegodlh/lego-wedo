#!/usr/bin/env python
# -*- coding: utf-8 -*-
# TODO
# No está reconociendo Ctrl+Alt+E como salida
import time
import csv
import pdb
import win32api
import win32con
import win32gui
import config
import subprocess
import psutil
from projectSaver import EventHandler
from watchdog.observers import Observer
import os
import sys

# path to data gathered in experimental sessions
exp_data = sys.path[0] + '/../misc/exp-data'
# time in seconds to wait before checking if process was successfully closed
# at the end
wait_for_close = 5
refocus_latency = .150

mouseDown_events = ['mouseLeftDown', 'mouseRightDown', 'mouseMiddleDown']
mouseUp_events = ['mouseLeftUp', 'mouseRightUp', 'mouseMiddleUp']
mouse_events = ['mouseMove'] + mouseDown_events + mouseUp_events
keyDown_events = ['keyDown', 'keySysDown']
keyUp_events = ['keyUp', 'keySysUp']
keybd_events = keyDown_events + keyUp_events

screen_width = win32api.GetSystemMetrics(0)
screen_height = win32api.GetSystemMetrics(1)

projects_path = os.path.expanduser('~') +\
                '\Documents\LEGO Creations\WeDo\Projects'
min_reload_interval = 2

patches = [{'x1': m[1], 'y1': m[2], 'x2': m[3], 'y2': m[4]} for m in config.masks]
patches.append({'x1': 911, 'y1': 0, 'x2': 1366, 'y2': 384})  # player coords
patch_pixels = []
for p in patches:
    for x in range(p['x1'], p['x2']):
        for y in range(p['y1'], p['y2']):
            patch_pixels.append((x, y))
patch_pixels = set(patch_pixels)


def GetForegroundHandle():
    import win32gui
    return win32gui.GetForegroundWindow()


def GetForegroundTitle():
    import win32gui
    return win32gui.GetWindowText(GetForegroundHandle())


def MoveTo(dest):
    """Moves the mouse cursor to the (x,y) coordinates specified"""
    # source = win32api.GetCursorPos()
    # win32api.mouse_event(win32con.MOUSEEVENTF_MOVE,
    #                      dest[0] - source[0], dest[1] - source[1])
    # # Mode 1, with mouse_event:
    # win32api.mouse_event(win32con.MOUSEEVENTF_ABSOLUTE |
    #                      win32con.MOUSEEVENTF_MOVE,
    #                      int(dest[0]/(screen_width-1)*65535),
    #                      int(dest[1]/(screen_height-1)*65535))
    # Mode 2, with SetCursorPos:
    # At some point I thought mode 1 would be more accurate. But considering
    # that the mouse recorder uses GetCursorPos, maybe Mode 2 is more accurate.
    # I will try a side by side simulation (one on notebook one on pc) to seems
    # if there are major differences (though notebook and pc do behave
    # different however)
    win32api.SetCursorPos(dest)


def inputExitSignal():
    win32api.SetCursorPos((screen_width, 0))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    # Ctrl-Alt-R to remove eventual overlayers
    win32api.keybd_event(162, 0)
    win32api.keybd_event(164, 0)
    win32api.keybd_event(82, 0)
    win32api.keybd_event(162, 0, win32con.KEYEVENTF_KEYUP)
    win32api.keybd_event(164, 0, win32con.KEYEVENTF_KEYUP)
    win32api.keybd_event(82, 0, win32con.KEYEVENTF_KEYUP)
    # Ctrl-Alt-E to exit player
    win32api.keybd_event(162, 0)
    win32api.keybd_event(164, 0)
    win32api.keybd_event(69, 0)
    win32api.keybd_event(162, 0, win32con.KEYEVENTF_KEYUP)
    win32api.keybd_event(164, 0, win32con.KEYEVENTF_KEYUP)
    win32api.keybd_event(69, 0, win32con.KEYEVENTF_KEYUP)


def exitSafely(launcher_process):
    if not launcher_process.is_running():
        print('launcher closed successfully', end='; ', flush=True)
    else:
        print('killing launcher and children', end='; ', flush=True)
        try:
            launcher_children = launcher_process.children(recursive=True)
        except:
            print('failed to retrieve children', end='; ', flush=True)
        for child in launcher_children:
            if child.is_running():
                try:
                    child.kill()
                except:
                    print('failed to kill ' + child.name(), end='; ',
                          flush=True)
        try:
            launcher_process.kill()
        except:
            print('failed to kill launcher', end='; ', flush=True)
    for proc in psutil.process_iter():
        if proc.name() in ['AutoHotkeyU64.exe', 'WeDo.exe']:
            try:
                proc.kill()
            except:
                print('failed to kill ' + proc.name(), end='; ', flush=True)


def readKeylog(t0, record_t0, session_start, session_end, keylog_path,
               event_handler, soft_version, player_log):
    keylog = open(keylog_path)
    reader = list(csv.reader(keylog, dialect=csv.excel_tab))
    ts0 = session_start
    cursor = None
    keyDown_pos = None
    must_reload = False
    focus_lost = False
    fg_patch = None
    keys_down = set()
    last_reload = time.time()
    for i, row in enumerate(reader):
        msg = ''
        run = []
        ts = float(row[8])
        if i+1 != len(reader):
            ts_next = float(reader[i+1][8])
        if ts0 is None:
            ts0 = ts - 10  # mainly for testing purposes
        # on the output, timestamps are expressed
        # in terms of screen capture times
        msg += '{:.3f}: '.format(ts - record_t0)
        event = row[0]
        injection = int(row[10])
        event_handler.ts = ts - record_t0  # ts is unix timestamp of the
        # current event in the keylog; record_t0 is the unix timestamp which
        # corresponds to the screen capture video's t0; see video_info.ods in
        # the corresponding Dropbox directory
        if (ts >= ts0 and ts < session_end and
            event in mouse_events + keybd_events and not injection and
            not (event == 'mouseMove' and ts == ts_next)):
            if event in mouse_events:
                xPix = int(row[12])
                yPix = int(row[14])
                # before saving the cursor position into the cursor variable
                # it cuts off values exceeding the screen's dimensions
                if xPix < 0:
                    xPix = 0
                elif xPix >= screen_width:
                    xPix = screen_width - 1
                if yPix < 0:
                    yPix = 0
                elif yPix >= screen_height:
                    yPix = screen_height - 1
                cursor = (xPix, yPix)
                if keyDown_pos:
                    # reloads project if mouse moved more than text field
                    # dimensions after last keyDown event detected
                    if (abs(cursor[0] - keyDown_pos[0]) > 59 or
                        abs(cursor[1] - keyDown_pos[1]) > 29):
                        msg += 'moved far enough since last key down; '
                        must_reload = True
                        msg += 'reload requested; '
                        keyDown_pos = None
                if event == 'mouseMove':
                    # win32api.SetCursorPos((xPix, yPix))
                    # win32api.mouse_event(win32con.MOUSEEVENTF_MOVE,
                    #                      xPix - win32api.GetCursorPos()[0],
                    #                      yPix - win32api.GetCursorPos()[1])
                    run.append('MoveTo((xPix, yPix))')
                    msg += 'move '
                    msg += '%d, %d; ' % (xPix, yPix)
                elif event in mouseDown_events:
                    # win32api.SetCursorPos((xPix, yPix))
                    # win32api.mouse_event(win32con.MOUSEEVENTF_MOVE,
                    #                      xPix - win32api.GetCursorPos()[0],
                    #                      yPix - win32api.GetCursorPos()[1])
                    if win32api.GetCursorPos() != cursor:
                        run.append('MoveTo((xPix, yPix))')
                    if event == 'mouseLeftDown':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)')
                        msg += 'left down '
                    elif event == 'mouseRightDown':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN, 0, 0)')
                        msg += 'right down '
                    elif event == 'mouseMiddleDown':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_MIDDLEDOWN, 0, 0)')
                        msg += 'middle down '
                    msg += '%d, %d; ' % (xPix, yPix)
                    # run.append('win32api.SetCursorPos((xPix, yPix))')
                    focus_lost = False
                    fg_patch = None
                elif event in mouseUp_events:
                    # win32api.SetCursorPos((xPix, yPix))
                    # win32api.mouse_event(win32con.MOUSEEVENTF_MOVE,
                    #                      xPix - win32api.GetCursorPos()[0],
                    #                      yPix - win32api.GetCursorPos()[1])
                    if win32api.GetCursorPos() != cursor:
                        run.append('MoveTo((xPix, yPix))')
                    if event == 'mouseLeftUp':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)')
                        msg += 'left up '
                    elif event == 'mouseRightUp':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP, 0, 0)')
                        msg += 'right up '
                    elif event == 'mouseMiddleUp':
                        run.append('win32api.mouse_event(win32con.MOUSEEVENTF_MIDDLEUP, 0, 0)')
                        msg += 'middle up '
                    msg += '%d, %d; ' % (xPix, yPix)
                    # cursor position set again after mouse up
                    # it was noticed that otherwise hand grab icon remained
                    # unchanged after mouse up
                    # run.append('win32api.SetCursorPos((xPix, yPix))')
                    keyDown_pos = None
                    if cursor not in patch_pixels:
                        if GetForegroundTitle() == 'Editor.vi':
                            must_reload = True
                            msg += 'reload requested; '
            elif event in keybd_events:
                keyId = int(row[18])
                if event in keyDown_events:
                    keys_down.add(keyId)
                    if focus_lost:
                        # if project reloaded, focus will have been lost
                        # cursor removed and replaced to regain focus
                        # only once until focus is lost again
                        delta = time.time() - last_reload
                        msg += ('will refocus - {} milliseconds ellapsed since reload; '.format(int(delta*1000)))
                        if delta < refocus_latency:
                            time.sleep(refocus_latency - delta)
                        win32api.SetCursorPos((0, 0))
                        win32api.SetCursorPos(cursor)
                        msg += 'focus regained before key down; '
                        focus_lost = False
                    if fg_patch:
                        try:
                            win32gui.SetForegroundWindow(fg_patch)
                            msg += 'rising patch to foreground; '
                        except:
                            msg += 'unable to rise patch to foreground; '
                        fg_patch = None
                    if GetForegroundTitle() == 'Editor.vi':
                        msg += 'saving cursor position on key down; '
                        keyDown_pos = cursor
                    run.append('win32api.keybd_event(keyId, 0)')
                    msg += 'key down '
                elif event in keyUp_events:
                    keys_down.discard(keyId)
                    run.append('win32api.keybd_event(keyId, 0, win32con.KEYEVENTF_KEYUP)')
                    msg += 'key up '
                key = row[12]
                msg += key + '; '
        else:
            if ts < ts0:
                msg += 'too early; '
            elif ts >= session_end:
                msg += 'too late; '
            elif event not in mouse_events + keybd_events:
                msg += 'ignored event; '
            elif injection:
                msg += 'injected; '
            elif event == 'mouseMove' and ts == ts_next:
                msg += 'ignoring mouseMove followed by simultaneous event'
        while time.time() - t0 < ts - ts0:
            time.sleep(.001)
            pass
        for cmd in run:
            msg += str(time.time()) + '; '
            eval(cmd)
        if must_reload:
            if event_handler.current:
                if (time.time() - last_reload > min_reload_interval and
                    not keyDown_pos):
                    if GetForegroundTitle() != 'Editor.vi':
                        fg_patch = GetForegroundHandle()
                        msg += 'patch is active, saving handle; '
                    msg += 'reloading; '
                    subprocess.Popen([config.bin_path, event_handler.current])
                    last_reload = time.time()
                    must_reload = False
                    focus_lost = True
                else:
                    msg += 'reload request postponed; '
        # concatenates messages from event_handler and prints everything to file
        msg += event_handler.msg
        event_handler.msg = ''
        # print(msg)
        print(msg, file=player_log)
        msg = ''
    # cleans up any key or button that may have been left pressed
    for key in keys_down:
        win32api.keybd_event(key, 0, win32con.KEYEVENTF_KEYUP)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
    win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP, 0, 0)
    win32api.mouse_event(win32con.MOUSEEVENTF_MIDDLEUP, 0, 0)
    keylog.close()


def wipeProjectFolder():
    """Wipes the WeDo project folder contents"""
    from glob import glob
    from os import remove
    project_folder = projects_path
    for file in glob(project_folder + '\*'):
        remove(file)


def launchLauncher(uid, modenum, session_ts, soft_version, save_path):
    from os import path
    if soft_version < 5:
        reset_latency = '0.2'
    else:
        reset_latency = '0.5'
    p = subprocess.Popen(['launcher',
                          '--uid', uid, '--mode', str(modenum),
                          '--archive', 'move', '--custom-ts', session_ts,
                          '--timeout', '90', '--reset-latency', reset_latency,
                          '--save-path', save_path],
                         shell=True,
                         stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    return p


def playback(uid, mode, modenum, session_ts, record_t0, session_start, session_end,
             keylog_path, soft_version, save_path):
    """Initiates the file watcher, starts the launcher and reads and runs the
    keylog"""
    wipeProjectFolder()
    event_handler = EventHandler(save_path)
    observer = Observer()
    observer.schedule(event_handler, projects_path, recursive=False)
    observer.start()
    log_path = '{}/{}_{}_{}.playlog'.format(save_path, uid, mode, session_ts)
    player_log = open(log_path, 'w')
    t0 = time.time()
    try:
        event_handler.ts = -1  # defaults event handler timer to -1
        # print('launching', end='; ', flush=True)
        p = launchLauncher(uid, modenum, session_ts, soft_version, save_path)
        launcher_process = psutil.Process(p.pid)
        # print('reading log', end='; ', flush=True)
        readKeylog(t0, record_t0, session_start, session_end, keylog_path,
                   event_handler, soft_version, player_log)
    finally:
        observer.stop()
        player_log.close()
        inputExitSignal()
        time.sleep(wait_for_close)
        exitSafely(launcher_process)
    observer.join()


def markPlayedback(session_list, rownum):
    import csv
    with open(session_list) as filein:
        reader = csv.DictReader(filein)
        fields = reader.fieldnames
        tmp_list = session_list + '~'
        with open(tmp_list, 'w') as fileout:
            writer = csv.DictWriter(fileout, fields, lineterminator='\n')
            writer.writeheader()
            for r, rowin in enumerate(reader):
                rowout = rowin
                if r == rownum:
                    rowout['playback'] = 1
                writer.writerow(rowout)
    os.remove(session_list)
    os.rename(tmp_list, session_list)


def restartSessionList(session_list):
    import csv
    with open(session_list, 'r') as filein:
        reader = csv.DictReader(filein)
        fields = reader.fieldnames
        tmp_list = session_list + '~'
        with open(tmp_list, 'w') as fileout:
            writer = csv.DictWriter(fileout, fields, lineterminator='\n')
            writer.writeheader()
            for linein in reader:
                lineout = linein
                lineout['playback'] = 0
                writer.writerow(lineout)
    os.remove(session_list)
    os.rename(tmp_list, session_list)


def getKeylogPath(soft_version, stage, session_basename, record_ts, uid):
    if soft_version < 4:
        keylog_path = '/'.join([exp_data, stage, 'data', session_basename,
                                session_basename + '_keylog.csv'])
    else:
        record_basename = uid + '_' + record_ts
        keylog_path = '/'.join([exp_data, stage,
                                record_basename + '_keylog.csv'])
    return keylog_path


def getModeNum(mode):
    if mode == 'teach':
        modenum = 1
    elif mode == 'train':
        modenum = 2
    elif mode == 'test':
        modenum = 3
    elif mode == 'retest':
        modenum = 4
    return modenum


def getSessionStart(session_ts):
    # code lines below allow to obtain session start and end times
    # mouse/keyboard events are ignored until start time is achieved
    # and events after its end are ignored as well
    session_start = time.strptime(session_ts, '%y%m%d%H%M%S')
    session_start = time.mktime(session_start)
    # rounded up one second as microseconds were absent;
    # at the beginning of the session it is preferable to miss events than to
    # include extra ones:
    session_start += 1
    return session_start


def getSessionEnd(stage, session_basename, session_ts):
    # session end time is obtained from session's log
    session_log = '/'.join([exp_data, stage, 'data', session_basename,
                            session_basename + '.log'])
    end_time = None
    with open(session_log) as log:
        for line in log.readlines():
            event_time, event_flag = line.rstrip().split('\t')[:2]
            end_flags = ['time_up', 'revision', 'end']
            if event_flag in end_flags:
                end_time = event_time
                break
    if end_time is None:
        # if session's end time not available in session's log
        # the end time is defaulted to a very high number
        end_time = 9999999999
        print("session's end time not found", end='; ', flush=True)
    session_end = time.strptime(session_ts[:6] + end_time, '%y%m%d%H:%M:%S')
    # rounded up one second as microseconds were absent;
    # at the end of the session it is preferable to include extra events
    # than to miss informative ones:
    session_end = time.mktime(session_end) + 1
    return session_end


def scanSessionList(input, save_path):
    session_list = input
    with open(session_list) as filein:
        reader = list(csv.DictReader(filein))
    for s, session in enumerate(reader):
        stage = session['stage']
        uid = '{:03d}'.format(int(session['uid']))
        mode = session['mode']
        modenum = getModeNum(mode)
        record_ts = session['record_ts']
        session_ts = session['session_ts']
        soft_version = int(session['soft_version'])
        record_t0 = float(session['record_t0'])
        playedback = int(session['playback'])
        if (stage == 'E2' or stage == 'E4') and not playedback:
            # ignores sessions marked as playedback already (playback>=1)
            session_basename = uid + '_' + mode + '_' + session_ts
            print(session_basename, end='; ', flush=True)
            print('unavailable mem%: ' + str(psutil.virtual_memory().percent),
                  end='; ', flush=True)
            keylog_path = getKeylogPath(soft_version, stage, session_basename,
                                        record_ts, uid)
            session_start = getSessionStart(session_ts)
            session_end = getSessionEnd(stage, session_basename, session_ts)
            playback(uid, mode, modenum, session_ts, record_t0,
                     session_start, session_end, keylog_path, soft_version,
                     save_path)
            print()
        markPlayedback(input, s)


def runMultiple(input, save_path):
    from os import path
    from os import makedirs
    run = 0
    while True:
        run += 1
        run_path = save_path + '/' + 'run{:02d}'.format(run)
        while path.exists(run_path):
            run += 1
            run_path = save_path + '/' + 'run{:02d}'.format(run)
        os.makedirs(run_path)
        print(run_path)
        scanSessionList(input, run_path)
        restartSessionList(input)


def main(input, mode, save_path):
    from os import path
    from os import makedirs
    if not path.exists(save_path):
        makedirs(save_path)
    if mode == 1:
        if path.splitext(input)[1] == '.csv':
            scanSessionList(input, save_path)
        else:
            raise Exception('single run mode requires csv input')
    elif mode == 2:
        if path.splitext(input)[1] == '.csv':
            runMultiple(input, save_path)
        else:
            raise Exception('infinite run mode requires csv input')
    elif mode == 3:
        if path.splitext(input)[1] == '.csv':
            uid = 999
            mode = 'test'
            modenum = 3
            session_ts = 0
            record_t0 = 0
            session_start = None
            session_end = 9999999999
            keylog_path = input
            soft_version = 4
            playback(uid, mode, modenum, session_ts, record_t0, session_start,
                     session_end, keylog_path, soft_version, save_path)
        else:
            raise Exception('infinite run mode requires csv input')


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Keylog player')
    parser.add_argument('input',
                        help='input file, either keylog or csv session list')
    parser.add_argument('--mode', type=int, choices=[1, 2, 3], default=1,
                        help='run mode: 1. single run, 2. infinite runs, 3. single keylog')
    parser.add_argument('--save-path', default=sys.path[0] + '/../data')
    args = parser.parse_args()
    main(args.input, args.mode, args.save_path)
