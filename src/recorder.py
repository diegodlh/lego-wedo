#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pdb

# TODO:
# desactivar fácilmente streams no deseados
# No abrir un dispositivo si ya está abierto
# Los saltos de línea en el archivo log
# DISMINUIR EL USO DEL CPU DEL STREAM DE LA PANTALLA! Capaz no hace falta codificar dos veces

# es importante que la ruta esté expresada con barras Windows
save_to = 'd:\\videos\\'
devices = {
           'AD71': {'video': r'@device_pnp_\\?\usb#vid_046d&pid_082d&mi_00#6&38062dab&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\{bbefb6c7-2fc4-4139-bb8b-a58bba724083}',
                    'audio': r'Micrófono (HD Pro Webcam C920)',
                    'input_args': '-vcodec h264 -s 1920x1080 -framerate 24',
                    'file_args': '-copyinkf -vcodec copy',
                    'vidext': '.mp4',
                    'stream': 1,
                    'stream_args': '-vcodec mpeg4 -s 160x120 -r 6'},
           'E0B1': {'video': r'@device_pnp_\\?\usb#vid_046d&pid_082d&mi_00#6&875004e&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\{bbefb6c7-2fc4-4139-bb8b-a58bba724083}',
                    'audio': r'Micrófono (2- HD Pro Webcam C92',
                    'input_args': '-vcodec h264 -s 1920x1080 -framerate 24',
                    'file_args': '-copyinkf -vcodec copy',
                    'vidext': '.mp4',
                    'stream': 1,
                    'stream_args': '-vcodec mpeg4 -s 160x120 -r 6'},
           'scrn': {'video': 'screen-capture-recorder',
                    'audio': 'Micrófono (Realtek High Definit',
                    'input_args': '-r 12',
                    'file_args': '-vcodec libx264 -crf 0 -preset ultrafast',
                    'vidext': '.mp4',
                    'stream': 1,
                    'stream_args': '-vcodec mpeg4 -s 160x120 -r 6'},
        #    'ntbk': {'video': 'Lenovo EasyCamera',
        #             'audio': '',
        #             'input_args': '',
        #             'file_args': '-vcodec libx264 -r 24 -crf 0 -preset ultrafast',
        #             'vidext': '.mp4',
        #             'stream': 1,
        #             'stream_args': '-vcodec mpeg4 -s 160x120 -r 6'}
           }


def main():
    import subprocess
    from datetime import datetime
    import os
    from keyLogger import KeyLogger
    global ffmpegs
    global logs
    global vlc
    global key_logger
    if not os.path.exists(save_to):
        os.makedirs(save_to)
    timestamp = '{:%y%m%d%H%M%S}'.format(datetime.now())
    uid = '%03d_%s' % (int(input('id: ')), timestamp)
    mpegts_to = 'udp://localhost:'
    mpegts_from = 'udp://@:'
    rtsp_to = 'rtsp://:8554/'
    ffmpegs = []
    logs = []
    vlm_name = save_to + uid + '.vlm'
    vlm = open(vlm_name, 'w')
    # vlcs = []
    for i, dev in enumerate(devices.items()):
        name = dev[0]
        param = dev[1]
        port = 6660 + i
        namebase = save_to + uid + '_' + name
        param['vidname'] = namebase + param['vidext']
        logname = namebase + '.log'
        param['mpegts_to'] = mpegts_to + str(port)
        param['mpegts_from'] = mpegts_from + str(port)
        param['rtsp_to'] = rtsp_to + name
        ffmpeg_cmd = (('ffmpeg -rtbufsize 100M -f dshow %(input_args)s ' +
                       '-i video="%(video)s":audio="%(audio)s" ' +
                       '%(file_args)s %(vidname)s ') % param)
        if param['stream']:
            ffmpeg_cmd += ('%(stream_args)s -f mpegts %(mpegts_to)s' % param)
        # vlc_cmd = 'vlc %(mpegts_from)s --sout=#rtp{sdp=%(rtsp_to)s}' % param
        with open(logname, 'w') as log:
            # log.write(ffmpeg_cmd + '\n' + vlc_cmd + '\n')
            log.write(ffmpeg_cmd + '\n')
        logs.append(open(logname, 'a'))
        ffmpeg = subprocess.Popen(ffmpeg_cmd, stdout=logs[i], stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
        # vlc = subprocess.Popen(vlc_cmd)
        ffmpegs.append(ffmpeg)
        # vlcs.append(vlc)
        if param['stream']:
            vlm.write('new %s broadcast enabled\n' % name +
                      'setup %s input %s\n' % (name, param['mpegts_from']) +
                      'setup %s output #rtp{sdp=%s}\n' % (name,
                                                          param['rtsp_to']))

    for key, value in devices.items():
        if value['stream']:
            vlm.write('control %s play\n' % key)
    vlm.close()
    vlc_cmd = 'vlc -I dummy --dummy-quiet --vlm %s' % vlm_name
    # vlc_cmd = 'vlc -vvv --vlm %s' % vlm_name
    print(vlc_cmd)
    vlc = subprocess.Popen(vlc_cmd)
    key_logger = KeyLogger(save_to, uid)
    key_logger.start()

    key = None
    while key is not 'q':
        key = input("ingrese 'q' para finalizar...")


if __name__ == '__main__':
    import sys
    try:
        sys.exit(main())
    finally:
        for ffmpeg in ffmpegs:
            ffmpeg.communicate(b'q')
        for log in logs:
            log.close()
        vlc.kill()
        key_logger.stop()

# esto debería funcionar:
# ffmpeg -rtbufsize 100M -f dshow -vcodec h264 -s 1920x1080 -framerate 24 -i video="Logitech HD Pro Webcam C920" -map 0:v -map 0:v -copyinkf:v:0 -c:v:0 copy -c:v:1 mpeg4 -s 160x120 -r 6 -f tee "[select=\'v:0\']cam1.mp4|[select=\'v:1\':f=mpegts]udp://localhost:6661"
# pero no funciona porque por alguna razón dentro de tee el formato mp4 no funciona con el input que le estoy dando (fuera de tee sí.): Tag H264/0x34363248 incompatible with output codec id '28' ([33][0][0][0])
