#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pdb

# es importante que la ruta esté expresada con barras Windows
save_to = 'd:\\videos\\'
rtsp_to = 'rtsp://:8554/'


def main():
    from datetime import datetime
    timestamp = '{:%y%m%d%H%M%S}'.format(datetime.now())
    uid = '%03d_%s' % (int(input('id: ')), timestamp)
    record(uid)
    key = None
    while key is not 'q':
        key = input("ingrese 'q' para finalizar...")


def record(uid):
    import subprocess
    import os
    from keyLogger import KeyLogger
    global ffmpeg
    global log
    global vlc
    global key_logger
    if not os.path.exists(save_to):
        os.makedirs(save_to)
    ffmpeg_cmd = (r'ffmpeg -rtbufsize 100M ' +
                  r'-f dshow -vcodec h264 -s 1920x1080 -framerate 24 -i video="@device_pnp_\\?\usb#vid_046d&pid_082d&mi_00#6&875004e&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\{bbefb6c7-2fc4-4139-bb8b-a58bba724083}":audio="Micrófono (2- HD Pro Webcam C92" ' +
                  r'-f dshow -vcodec h264 -s 1920x1080 -framerate 24 -i video="@device_pnp_\\?\usb#vid_046d&pid_082d&mi_00#6&38062dab&0&0000#{65e8773d-8f56-11d0-a3b9-00a0c9223196}\{bbefb6c7-2fc4-4139-bb8b-a58bba724083}":audio="Micrófono (HD Pro Webcam C920)" ' +
                  r'-f dshow -r 12 -i video="screen-capture-recorder":audio="Micrófono (Realtek High Definit" ' +
                  r'-filter_complex "' +
                  r'[0]setpts=PTS-STARTPTS, framerate=6, scale=160x120, split[F][upleft]; ' +
                  r'[1]setpts=PTS-STARTPTS, framerate=6, scale=160x120, split[R][lowleft]; ' +
                  r'[2]setpts=PTS-STARTPTS, framerate=6, scale=320x240, split[S][right]; ' +
                  r'[2]asetpts=PTS-STARTPTS, asplit=4[mic0][mic1][mic2][mic3]; ' +
                  r'nullsrc=size=480x240[base]; ' +
                  r'[base][upleft]overlay[tmp0]; [tmp0][lowleft]overlay=y=120[tmp1]; [tmp1][right]overlay=x=160[out]" ' +
                  r'-map 0 -copyinkf -vcodec copy %s_front.mp4 ' % (save_to + uid) +
                  r'-map 1 -copyinkf -vcodec copy %s_rear.mp4 ' % (save_to + uid) +
                  r'-map 2 -vcodec libx264 -crf 0 -preset ultrafast %s_screen.mp4 ' % (save_to + uid) +
                  r'-map [F] -vcodec mpeg4 -map [mic0] -f mpegts udp://localhost:6660 ' +
                  r'-map [R] -vcodec mpeg4 -map [mic1] -f mpegts udp://localhost:6661 ' +
                  r'-map [S] -vcodec mpeg4 -map [mic2] -f mpegts udp://localhost:6662 ' +
                  r'-map [out] -vcodec mpeg4 -map [mic3] -f mpegts udp://localhost:6666')
    logname = save_to + uid + '.log'
    with open(logname, 'w') as log:
        log.write(ffmpeg_cmd + '\n')
    log = open(logname, 'a')
    ffmpeg = subprocess.Popen(ffmpeg_cmd, stdout=log, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    vlmname = save_to + uid + '.vlm'
    with open(vlmname, 'w') as vlm:
        vlm.write('new front broadcast enabled\nsetup front input udp://@:6660\nsetup front output #rtp{sdp=%sfront}\n' % rtsp_to +
                  'new rear broadcast enabled\nsetup rear input udp://@:6661\nsetup rear output #rtp{sdp=%srear}\n' % rtsp_to +
                  'new scrn broadcast enabled\nsetup scrn input udp://@:6662\nsetup scrn output #rtp{sdp=%sscrn}\n' % rtsp_to +
                  'new full broadcast enabled\nsetup full input udp://@:6666\nsetup full output #rtp{sdp=%sfull}\n' % rtsp_to +
                  'control front play\ncontrol rear play\ncontrol scrn play\ncontrol full play\n')
    vlc_cmd = ('vlc -I dummy --dummy-quiet --vlm %s' % vlmname)
    print(vlc_cmd)
    vlc = subprocess.Popen(vlc_cmd)
    key_logger = KeyLogger(save_to, uid)
    key_logger.start()


def cleanup():
    ffmpeg.communicate(b'q')
    log.close()
    # vlc.kill()
    key_logger.stop()


if __name__ == '__main__':
    import sys
    try:
        sys.exit(main())
    finally:
        cleanup()